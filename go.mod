module simp

go 1.18

require (
	github.com/emicklei/dot v1.0.0
	github.com/gen2brain/raylib-go/raylib v0.0.0-20221031152736-892ce4892cf3
	gotest.tools v2.2.0+incompatible
)

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
