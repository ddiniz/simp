package main

import (
	ed "simp/source/editor"
	"simp/source/enums"
	rp "simp/source/rope"
	"simp/source/utils"

	rl "github.com/gen2brain/raylib-go/raylib"
)

func main() {
	programArguments := parseArguments()
	str := ""
	if programArguments.Filename.Value != "" {
		str = utils.FileToString(programArguments.Filename.Value)
	}
	mainEditor := ed.CreateEditor(str)

	renderedText, err := rp.ToString(mainEditor.CurrentTab.Rope)
	utils.PrintError(err)

	rl.InitWindow(800, 450, "Simp Text Editor")

	font := rl.LoadFontEx("./VictorMono-Regular.ttf", 32, nil)
	rl.GenTextureMipmaps(&font.Texture)
	rl.SetTextureFilter(font.Texture, rl.FilterPoint)

	FPS := int32(60)
	rl.SetTargetFPS(FPS)
	cursorBlinkTimer := FPS
	for !rl.WindowShouldClose() {
		rl.BeginDrawing()
		rl.ClearBackground(rl.Black)
		hadInput := input(*mainEditor)

		if hadInput {
			renderedText, err = rp.ToString(mainEditor.CurrentTab.Rope)
			utils.PrintError(err)
		}
		rl.DrawTextEx(font, renderedText, rl.Vector2{0, 0}, 32, 0, rl.White)

		drawCursors(&cursorBlinkTimer, FPS, *mainEditor)
		rl.EndDrawing()
	}
	rl.UnloadFont(font)
	rl.CloseWindow()
}

func drawCursors(cursorBlinkTimer *int32, FPS int32, mainEditor ed.Editor) {
	fontWidth := int32(14)
	fontHeigth := int32(30)
	if *cursorBlinkTimer <= FPS/2 {
		for _, cur := range mainEditor.CurrentTab.Cursors {
			rl.DrawRectangle(
				int32(cur.Col)*fontWidth,
				int32(cur.Row)*fontHeigth,
				fontWidth,
				fontHeigth,
				rl.White,
			)
		}
	}
	if *cursorBlinkTimer <= 0 {
		*cursorBlinkTimer = FPS
	}
	*cursorBlinkTimer -= 1
}

func input(editor ed.Editor) bool {
	key := rl.GetKeyPressed()
	switch key {
	case rl.KeyEnter:
		err := ed.AddTextRight(editor, "\n")
		utils.PrintError(err)
	case rl.KeyBackspace:
	case rl.KeyDelete:

	case rl.KeyUp:
		ed.MoveCursors(&editor, enums.UP)
	case rl.KeyDown:
		ed.MoveCursors(&editor, enums.DOWN)
	case rl.KeyLeft:
		ed.MoveCursors(&editor, enums.LEFT)
	case rl.KeyRight:
		ed.MoveCursors(&editor, enums.RIGHT)

	case rl.KeyTab:
	case rl.KeyLeftAlt:
	case rl.KeyLeftControl:
	case rl.KeyLeftShift:
	case rl.KeyRightAlt:
	case rl.KeyRightControl:
		print("Structure to Graph\n")
		utils.RopeStructureToGraph(editor.CurrentTab.Rope)
	case rl.KeyRightShift:

	}
	keyUnicode := rl.GetCharPressed()
	hadInput := false
	// Check if more characters have been pressed on the same frame
	for keyUnicode > 0 {
		if keyUnicode >= 32 && keyUnicode <= 256 {
			err := ed.AddTextRight(editor, string(keyUnicode))
			utils.PrintError(err)
		}
		keyUnicode = rl.GetCharPressed() // Check next character in the queue
		if !hadInput {
			hadInput = true
		}
	}
	return hadInput
}
