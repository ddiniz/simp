package main

import (
	"flag"
	"reflect"
)

type StringArgument struct {
	Name      string
	Shorthand string
	Usage     string
	Value     string
}

type ProgramArguments struct {
	Filename StringArgument
}

func getField(programArgs *ProgramArguments, field string) StringArgument {
	fieldRef := reflect.ValueOf(programArgs)
	fieldVal := reflect.Indirect(fieldRef).FieldByName(field).Interface().(StringArgument)
	return fieldVal
}

func parseArguments() ProgramArguments {
	program := ProgramArguments{}
	program.Filename = StringArgument{"filename", "f", "Source file", ""}
	flag.StringVar(&program.Filename.Value, program.Filename.Name, program.Filename.Value, program.Filename.Usage)
	flag.StringVar(&program.Filename.Value, program.Filename.Shorthand, program.Filename.Value, program.Filename.Usage)
	/* fields := reflect.VisibleFields(reflect.TypeOf(struct{ ProgramArguments }{}))
	for _, field := range fields {
		if field.Name == "ProgramArguments" {
			continue
		}
		arg := getField(&program, field.Name)
		// Setting the value of the argument to the value of the flag.
		flag.StringVar(&arg.Value, arg.Name, arg.Value, arg.Usage)
		flag.StringVar(&arg.Value, arg.Shorthand, arg.Value, arg.Usage)
	} */
	flag.Parse()
	return program
}
