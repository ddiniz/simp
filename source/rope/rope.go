package rope

import (
	"fmt"
	"math"
	enums "simp/source/enums"
)

var DEFAULT_ROPE_NODE_MAX_LENGTH = 1000
var DEFAULT_ROPE_NODE_JOIN_LENGTH = 500
var DEFAULT_ROPE_REBALANCE_RATIO = 1.2

type RopeContainer struct {
	RopeNodeMaxLength  int
	RopeNodeJoinLength int
	RopeRebalanceRatio float64
	Cursors            []*Cursor
	Rope               *Rope
}

type Cursor struct {
	Row int
	Col int
}

type Rope struct {
	Value      string
	Length     int
	Linebreaks int
	Left       *Rope
	Right      *Rope
}

func CreateRopeContainer(text string) *RopeContainer {
	return CreateRopeContainerWithOptions(text, DEFAULT_ROPE_NODE_MAX_LENGTH, DEFAULT_ROPE_NODE_JOIN_LENGTH, DEFAULT_ROPE_REBALANCE_RATIO)
}
func CreateRopeContainerWithOptions(text string, RopeNodeMaxLength int, RopeNodeJoinLength int, RopeRebalanceRatio float64) *RopeContainer {
	cont := &RopeContainer{}
	cont.RopeNodeMaxLength = RopeNodeMaxLength
	cont.RopeNodeJoinLength = RopeNodeJoinLength
	cont.RopeRebalanceRatio = RopeRebalanceRatio
	cont.AddCursor(0, 0)
	cont.Rope = cont.CreateRope(text)
	return cont
}

func (cont RopeContainer) CreateRope(text string) *Rope {
	cont.Rope = &Rope{}
	cont.Rope.Value = text
	cont.Rope.Length = len(text)
	//rope.Linebreaks = strings.Count(text, "\n")
	return cont.AdjustRope(cont.Rope)
}

func (cont RopeContainer) AdjustRope(rope *Rope) *Rope {
	if rope == nil {
		return nil
	}
	if rope.Left == nil && rope.Right == nil {
		if rope.Length > cont.RopeNodeMaxLength {
			half := int(math.Floor(float64(rope.Length) / float64(2)))
			rope.Left = cont.CreateRope(rope.Value[:half])
			rope.Right = cont.CreateRope(rope.Value[half:])
			rope.Value = ""
		}
	} else {
		if rope.Length < cont.RopeNodeJoinLength {
			rope.Value = first(ToString(rope.Left)) + first(ToString(rope.Right))
			rope.Left = nil
			rope.Right = nil
		}
	}
	return rope
}

func (cont RopeContainer) Insert(rope *Rope, position int, text string) error {
	if rope == nil {
		return nil
	}
	if position < 0 || position > rope.Length {
		return fmt.Errorf("trying to insert text in a position (%d) beyond rope bounds (0-%d)", position, rope.Length)
	}
	if rope.Right == nil && rope.Left == nil {
		rope.Value = rope.Value[:position] + text + rope.Value[position:]
		rope.Length = len(rope.Value)
	} else {
		leftLength := rope.Left.Length
		if position < leftLength {
			cont.Insert(rope.Left, position, text)
			rope.Length = rope.Left.Length + rope.Right.Length
		} else {
			cont.Insert(rope.Right, position-leftLength, text)
		}
	}
	cont.AdjustRope(rope)
	return nil
}

func (cont RopeContainer) Remove(rope *Rope, start, end int) error {
	if rope == nil {
		return nil
	}
	if start < 0 || start > rope.Length || end < 0 || end > rope.Length {
		return fmt.Errorf("trying to remove text beyond rope bounds. Removing between %d-%d, rope is 0-%d", start, end, rope.Length)
	}
	if rope.Right == nil && rope.Left == nil {
		rope.Value = rope.Value[0:start] + rope.Value[end:]
		rope.Length = len(rope.Value)
	} else {
		leftLength := rope.Left.Length
		leftStart, leftEnd := getLeftStartAndLeftEnd(leftLength, start, end)
		rightStart, rightEnd := getRightStartAndRightEnd(leftLength, rope.Right.Length, start, end)
		if leftStart < leftLength {
			cont.Remove(rope.Left, leftStart, leftEnd)
		}
		if rightEnd > 0 {
			cont.Remove(rope, rightStart, rightEnd)
		}
		rope.Length = rope.Left.Length + rope.Right.Length
	}
	cont.AdjustRope(rope)
	return nil
}

func (cont RopeContainer) Rebuild(rope *Rope) {
	if rope == nil || rope.Right == nil && rope.Left == nil {
		return
	}
	rope.Value = first(ToString(rope.Left)) + first(ToString(rope.Right))
	rope.Left = nil
	rope.Right = nil
	cont.AdjustRope(rope)
}

func (cont RopeContainer) Rebalance(rope *Rope) {
	if rope == nil || rope.Right == nil && rope.Left == nil {
		return
	}
	ratioLR := float64(rope.Left.Length) / float64(rope.Right.Length)
	ratioRL := float64(rope.Right.Length) / float64(rope.Left.Length)
	if ratioLR > cont.RopeRebalanceRatio || ratioRL > cont.RopeRebalanceRatio {
		cont.Rebuild(rope)
	} else {
		cont.Rebalance(rope.Left)
		cont.Rebalance(rope.Right)
	}

}

func Substring(rope *Rope, start int, endVar ...int) string {
	if rope == nil {
		return ""
	}
	end := 0
	if len(endVar) > 0 {
		end = min(endVar[0], rope.Length)
	}
	start = max(0, start)
	if rope.Right == nil && rope.Left == nil {
		return rope.Value[start:end]
	} else {
		leftLength := rope.Left.Length
		leftStart, leftEnd := getLeftStartAndLeftEnd(leftLength, start, end)
		rightStart, rightEnd := getRightStartAndRightEnd(leftLength, rope.Right.Length, start, end)
		if leftStart != leftEnd {
			if rightStart != rightEnd {
				return Substring(rope.Left, leftStart, leftEnd) + Substring(rope.Right, rightStart, rightEnd)
			} else {
				return Substring(rope.Left, leftStart, leftEnd)
			}
		} else {
			if rightStart != rightEnd {
				return Substring(rope.Right, rightStart, rightEnd)
			} else {
				return ""
			}
		}
	}
}

func SubstringN(rope *Rope, start int, length int) string {
	if rope == nil {
		return ""
	}
	start = max(0, start)
	length = min(length, rope.Length)
	end := start + length
	return Substring(rope, start, end)
}

func CharAt(rope *Rope, index int) string {
	if rope == nil {
		return ""
	}
	return Substring(rope, index, index+1)
}

func RuneAt(rope *Rope, index int) rune {
	if rope == nil {
		return -1
	}
	return []rune(Substring(rope, index, index+1))[0]
}

func (cont *RopeContainer) AddCursor(col int, row int) {
	cont.Cursors = append(cont.Cursors, &Cursor{col, row})
}

func (cont *RopeContainer) MoveCursors(dir enums.Direction) {
	for _, cur := range cont.Cursors {
		switch dir {
		case enums.UP:
			if cur.Row > 0 {
				cur.Row -= 1
			}
		case enums.RIGHT:
			cur.Col += 1
		case enums.LEFT:
			if cur.Col > 0 {
				cur.Col -= 1
			}
		case enums.DOWN:
			cur.Row += 1
		}
	}
}

func getLeftStartAndLeftEnd(leftLength int, start int, end int) (int, int) {
	return min(start, leftLength), max(end, leftLength)
}

func getRightStartAndRightEnd(leftLength int, rightLength int, start int, end int) (int, int) {
	return max(0, min(start-leftLength, rightLength)), max(0, min(end-leftLength, rightLength))
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func ToString(rope *Rope) (string, error) {
	if rope == nil {
		return "", fmt.Errorf("passing nil rope reference")
	}
	if rope.Right == nil && rope.Left == nil {
		return rope.Value, nil
	} else {
		return first(ToString(rope.Left)) + first(ToString(rope.Right)), nil
	}
}

func first[T, U any](val T, _ U) T {
	return val
}
