package rope

import (
	"testing"

	"gotest.tools/assert"
)

func TestCreateRopeContainer(t *testing.T) {
	texto := "texto"
	ropeContainer := CreateRopeContainer(texto)
	if ropeContainer.Rope == nil {
		assert.Error(t, nil, "CreateRopeContainer has a nil Rope ")
	}
	assert.Equal(t, ropeContainer.Rope.Length, len(texto))
	assert.Equal(t, len(ropeContainer.Cursors), 1)
	assert.Equal(t, ropeContainer.RopeNodeMaxLength, DEFAULT_ROPE_NODE_MAX_LENGTH)
	assert.Equal(t, ropeContainer.RopeNodeJoinLength, DEFAULT_ROPE_NODE_JOIN_LENGTH)
	assert.Equal(t, ropeContainer.RopeRebalanceRatio, DEFAULT_ROPE_REBALANCE_RATIO)
}

func TestCreateRopeContainerWithOptions(t *testing.T) {
	texto := "texto"
	ropeContainer := CreateRopeContainerWithOptions(texto, 2, 1, 1.2)
	if ropeContainer.Rope == nil {
		assert.Error(t, nil, "CreateRopeContainerWithOptions has a nil Rope ")
	}
	assert.Equal(t, ropeContainer.Rope.Length, len(texto))
	assert.Equal(t, len(ropeContainer.Cursors), 1)
	assert.Equal(t, ropeContainer.RopeNodeMaxLength, 2)
	assert.Equal(t, ropeContainer.RopeNodeJoinLength, 1)
	assert.Equal(t, ropeContainer.RopeRebalanceRatio, 1.2)
}

func TestCreateRope(t *testing.T) {
	texto := "textoo"
	ropeContainer := CreateRopeContainerWithOptions(texto, 2, 1, 1.2)
	if ropeContainer.Rope == nil {
		assert.Error(t, nil, "CreateRopeContainerWithOptions has a nil Rope ")
	}
	assert.Equal(t, ropeContainer.Rope.Length, len(texto))
	if ropeContainer.Rope.Left == nil {
		assert.Error(t, nil, "Left Rope should exist here")
	}
	if ropeContainer.Rope.Right == nil {
		assert.Error(t, nil, "Right Rope should exist here")
	}
	assert.Equal(t, ropeContainer.Rope.Left.Length, 3)
	assert.Equal(t, ropeContainer.Rope.Right.Length, 3)

	if ropeContainer.Rope.Left.Left == nil {
		assert.Error(t, nil, "Left Left Rope should exist here")
	}
	if ropeContainer.Rope.Left.Right == nil {
		assert.Error(t, nil, "Left Right Rope should exist here")
	}
	assert.Equal(t, ropeContainer.Rope.Left.Left.Length, 1)
	assert.Equal(t, ropeContainer.Rope.Left.Right.Length, 2)
	assert.Equal(t, ropeContainer.Rope.Left.Left.Value, "t")
	assert.Equal(t, ropeContainer.Rope.Left.Right.Value, "ex")

	if ropeContainer.Rope.Right.Left == nil {
		assert.Error(t, nil, "Right Left Rope should exist here")
	}
	if ropeContainer.Rope.Right.Right == nil {
		assert.Error(t, nil, "Right Right Rope should exist here")
	}
	assert.Equal(t, ropeContainer.Rope.Right.Left.Length, 1)
	assert.Equal(t, ropeContainer.Rope.Right.Right.Length, 2)
	assert.Equal(t, ropeContainer.Rope.Right.Left.Value, "t")
	assert.Equal(t, ropeContainer.Rope.Right.Right.Value, "oo")
}
