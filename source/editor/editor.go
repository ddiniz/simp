package editor

import (
	"fmt"
	enums "simp/source/enums"
	rp "simp/source/rope"
)

type Editor struct {
	Tabs       []*rp.RopeContainer
	CurrentTab *rp.RopeContainer
}

type Operation int

const (
	ADD Operation = 0
	DEL_LEFT
	DEL_RIGHT
)

type Undo struct {
	Operation Operation
	Text      string
	Index     int64
}

func CreateEditor(str string) *Editor {
	mainEditor := &Editor{}
	ropeCont := rp.CreateRopeContainer(str)
	mainEditor.CurrentTab = ropeCont
	mainEditor.Tabs = append(mainEditor.Tabs, ropeCont)
	return mainEditor
}

func AddTextLeft(ed Editor, text string) error {
	return AddText(ed, text, enums.LEFT)
}
func AddTextRight(ed Editor, text string) error {
	return AddText(ed, text, enums.RIGHT)
}
func AddText(ed Editor, text string, direction enums.Direction) error {
	if ed.CurrentTab == nil {
		return fmt.Errorf("current tab is nil")
	}
	if len(ed.CurrentTab.Cursors) == 0 {
		return fmt.Errorf("current tab has no cursors")
	}
	for _, cur := range ed.CurrentTab.Cursors {
		ed.CurrentTab.Insert(ed.CurrentTab.Rope, calcIndex(cur.Col, cur.Row), text)
		cur.Col += len(text)
	}

	return nil
}

func DelTextLeft(ed Editor, row int, col int) {
	DelText(ed, row, col, enums.LEFT)
}
func DelTextRight(ed Editor, row int, col int) {
	DelText(ed, row, col, enums.RIGHT)
}
func DelText(ed Editor, row int, col int, direction enums.Direction) error {
	if ed.CurrentTab == nil {
		return fmt.Errorf("current tab is nil")
	}
	return nil
}

func calcIndex(col int, row int) int {
	return col + row
}

func AddCursor(ed *Editor, col int, row int) {
	ed.CurrentTab.AddCursor(col, row)
}

func MoveCursors(ed *Editor, dir enums.Direction) {
	ed.CurrentTab.MoveCursors(dir)
}
