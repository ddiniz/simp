package utils

import (
	"log"
	"os/exec"
	rp "simp/source/rope"

	"fmt"
	"os"

	"github.com/emicklei/dot"
)

func RopeStructureToGraph(rope *rp.Rope) {
	graph := dot.NewGraph(dot.Directed)
	//graph.Attr("rankdir", "RL")
	i := 0
	node := generateRopeNode(graph, rope, i)
	i++
	RopeWalk(graph, node, rope.Left, rope.Right, &i)
	StringToFile("./rope.gv", graph.String())
	GVtoSVG("./rope.gv")
}

func RopeWalk(graph *dot.Graph, parentNode dot.Node, ropeL *rp.Rope, ropeR *rp.Rope, i *int) {
	if ropeL != nil {
		node := generateRopeNode(graph, ropeL, *i)
		graph.EdgeWithPorts(parentNode, node, "left", "")
		*i++
		RopeWalk(graph, node, ropeL.Left, ropeL.Right, i)
	}
	if ropeR != nil {
		node := generateRopeNode(graph, ropeR, *i)
		graph.EdgeWithPorts(parentNode, node, "right", "")
		*i++
		RopeWalk(graph, node, ropeR.Left, ropeR.Right, i)
	}

}

func generateRopeNode(graph *dot.Graph, rope *rp.Rope, i int) dot.Node {
	node := graph.Node(fmt.Sprintf("{%d|VAL|%s|LEN|%d|{<left>L|<right>R}}", i, rope.Value, rope.Length))
	node.Attr("shape", "record")
	return node
}

func StringToFile(fileName string, content string) {
	f, err := os.Create("./" + fileName)
	if err != nil {
		panic(err)
	}
	_, err2 := f.WriteString(content)
	if err2 != nil {
		panic(err2)
	}
	f.Close()
}

func FileToString(fileName string) string {
	b, err := os.ReadFile(fileName)
	if err != nil {
		panic(err)
	}
	return string(b)
}

func PrintError(err error) {
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
}

func GVtoSVG(filename string) {
	cmd := exec.Command("dot", "-Tsvg", "-O", filename)
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}
