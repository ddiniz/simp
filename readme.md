# Prerequisites

Golang 1.18

# Run

```
go run source/cmd/app/main.go

# Linting

```
$ golangci-lint run ./...
```

# Build

```
go build -tags=prod source/cmd/app/main.go
```